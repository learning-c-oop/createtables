﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateTables.Tables
{
    class Table
    {
        public byte Row { get; set; }
        public byte Column { get; set; }
        public void Draw()
        {
            Console.WriteLine("Drawing The Table");
            for (int i = 0; i < Column; i++)
            {
                Console.Write(" " + new string('_', 3));
            }
            Console.WriteLine();
            for (int i = 0; i < Row; i++)
            {
                Console.Write("|");
                for (int j = 0; j < Column; j++)
                {
                    DrawCell();
                }
                Console.WriteLine();
            }
        }
        private void DrawCell()
        {
            Console.Write("___|");
        }
    }
}
