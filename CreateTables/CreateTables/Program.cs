﻿using CreateTables.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateTables
{
    class Program
    {
        static void Main(string[] args)
        {
            Table table = new Table { Row = 7, Column = 7 };
            table.Draw();

            Console.ReadKey();
        }
    }
}
